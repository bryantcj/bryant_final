﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEditor.SceneManagement;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    public Animator animator;
    public GameObject gem;
    public Text restartText;

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public GameObject playerExplosion;
    public GameObject gemRain;

    SpriteRenderer Foxy;

    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;
    private Vector3 startLocation;
   // private bool win = false;

    private AudioSource aSource;
    public AudioClip explosionClip;
    public AudioClip jumpClip;
    public AudioClip chestClip;






    private bool alive = true;

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        startLocation = transform.position;
        Foxy = GetComponent<SpriteRenderer>();
        aSource = GetComponent<AudioSource>();
        restartText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        animator.SetBool("Jump", !grounded);
        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }
        if (Input.GetKeyDown(KeyCode.R) && !alive)
        {
            alive = true;
            transform.position = startLocation;
            Foxy.enabled = true;
            rb2d.WakeUp();
            restartText.text = "";
        }
       // if (transform.position.y < -)
       // {
       //     alive = false;
       //     Foxy.enabled = false;

       // }
    }

    void FixedUpdate()
    {
        if (alive)
        {
            float h = Input.GetAxis("Horizontal");
            animator.SetFloat("Speed", Mathf.Abs(h));


            if (h * rb2d.velocity.x < maxSpeed)
                rb2d.AddForce(Vector2.right * h * moveForce);

            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

            if (h > 0 && !facingRight)
            {

                Flip();
            }

            else if (h < 0 && facingRight)
            {
                Flip();
            }

            if (jump)
            {
                
                rb2d.AddForce(new Vector2(0f, jumpForce));
                jump = false;
            }
        }
        
        
    }


    void Flip()
    {
        float h = Input.GetAxis("Horizontal");
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        animator.SetFloat("Speed", -h);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("Death"))
        {
            alive = false;
            Foxy.enabled = false;
            rb2d.Sleep();
            Instantiate(playerExplosion, transform.position, transform.rotation);
            aSource.clip = explosionClip;
            aSource.Play();
            Death();
        }
        if (other.CompareTag("Chest"))
        {
            Instantiate(gemRain, new Vector3(other.transform.position.x, other.transform.position.y + 10, 0), transform.rotation);
            aSource.clip = chestClip;
            aSource.Play();
        }
        if (other.CompareTag("Star"))
        {
            restartText.text = "Congratulations!";
            //win = true;
            Invoke("YouWin", 5f);

        }
        if (other.CompareTag("Level1"))
        {
            
            SceneManager.LoadScene("Level1");
        }
    }
   void YouWin()
   {
        SceneManager.LoadScene("Hub");
   }
  
    void Death()
    {
        restartText.text = "Press 'R' to Restart";
    }
}
